import { io } from "socket.io-client";

const URL = import.meta.env.VITE_SOCKET_HOST ?? "http://localhost:3000";
console.log(URL);

export const socket = io(URL);
