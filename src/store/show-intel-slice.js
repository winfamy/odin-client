import { createSlice } from "@reduxjs/toolkit";

const initialState = false;

const intelSlice = createSlice({
  name: "showIntel",
  initialState,
  reducers: {
    toggleShowIntel: (state, action) => {
      state = !state;
      return state;
    },
  },
});

export const { toggleShowIntel } = intelSlice.actions;

export default intelSlice.reducer;
