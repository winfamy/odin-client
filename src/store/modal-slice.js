import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { socket } from "../socket.js";
import moment from "moment";

const initialState = {
  modalOpen: "",
  launch: {},
  // system: {},
  conjunction: {},
  reentry: {},
};

const modalSlice = createSlice({
  name: "modal",
  initialState,
  reducers: {
    handleLaunchItemClick: (state, action) => {
      let { launch_id } = action.payload;
      state.launch_id = launch_id;
      state.modalOpen = "launch";
    },
    handleReentryClick: (state, action) => {
      let { reentry_id } = action.payload;
      state.reentry_id = reentry_id;
      state.modalOpen = "reentry";
    },
    handleConjunctionClick: (state, action) => {
      let { conjunction_id } = action.payload;
      state.conjunction_id = conjunction_id;
      state.modalOpen = "conjunction";
    },
    modalClose: (state, action) => {
      state.modalOpen = "";
    },
    modalOpen: (state, action) => {
      let { modalOpen } = action.payload;
      state.modalOpen = modalOpen;
    },
  },
});

export const {
  handleLaunchItemClick,
  handleReentryClick,
  handleConjunctionClick,
  modalClose,
  modalOpen,
} = modalSlice.actions;

export default modalSlice.reducer;
