import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  value: "us",
};

const countrySlice = createSlice({
  name: "country",
  initialState,
  reducers: {
    setCountry(state, action) {
      const country = action.payload;
      state.value = country;
    },
  },
});

export const { setCountry } = countrySlice.actions;

export default countrySlice.reducer;
