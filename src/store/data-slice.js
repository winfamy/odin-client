import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { socket } from "../socket.js";
import moment from "moment";

const initialState = {
  launches: [],
  systems: [],
  conjunctions: [],
  reentries: [],
};

export const addData = createAsyncThunk(
  "data/addData",
  async ({ data, country }) => {
    console.log({ data, country });
    socket.emit("add_data", { data, country });
    return {};
  }
);

export const updateSystem = createAsyncThunk(
  "data/updateSystem",
  async (data) => {
    socket.emit("update_system", data);
    return {};
  }
);

export const putLaunchIntel = createAsyncThunk(
  "data/putLaunchIntel",
  async (data) => {
    socket.emit("put_launch_intel", data);
    return data;
  }
);

export const putReentryIntel = createAsyncThunk(
  "data/putReentryIntel",
  async (data) => {
    socket.emit("put_reentry_intel", data);
    return data;
  }
);

export const putConjunctionIntel = createAsyncThunk(
  "data/putConjunctionIntel",
  async (data) => {
    socket.emit("put_conjunction_intel", data);
    return data;
  }
);

export const createLaunchFromModal = createAsyncThunk(
  "data/createLaunchFromModal",
  async ({ launch, country }) => {
    socket.emit("create_launch", { launch, country });
    return { launch, country };
  }
);

export const createConjunctionFromModal = createAsyncThunk(
  "data/createConjunctionFromModal",
  async ({ conjunction, country }) => {
    socket.emit("create_conjunction", { conjunction, country });
    return { conjunction, country };
  }
);

export const createReentryFromModal = createAsyncThunk(
  "data/createReentryFromModal",
  async ({ reentry, country }) => {
    socket.emit("create_reentry", { reentry, country });
    return { reentry, country };
  }
);

function doesIdExistInArray(array, id) {
  return array.filter((item) => item.id === id).length > 0;
}

const dataSlice = createSlice({
  name: "data",
  initialState,
  reducers: {
    setData: (state, action) => {
      state.launches = action.payload.launches;
      state.reentries = action.payload.reentries;
      state.conjunctions = action.payload.conjunctions;
      state.systems = action.payload.systems;

      state.launches.sort((a, b) => {
        return moment(a.timestamp) - moment(b.timestamp);
      });
      state.reentries.sort((a, b) => {
        return moment(a.timestamp) - moment(b.timestamp);
      });
      state.conjunctions.sort((a, b) => {
        return moment(a.timestamp) - moment(b.timestamp);
      });
    },

    createLaunch: (state, action) => {
      let { launch } = action.payload;
      if (!doesIdExistInArray(state.launches, launch.id)) {
        state.launches.push(launch);
        state.launches.sort((a, b) => {
          return moment(a.timestamp) - moment(b.timestamp);
        });
      }
    },

    createReentry: (state, action) => {
      let { reentry } = action.payload;
      if (!doesIdExistInArray(state.reentries, reentry.id)) {
        state.reentries.push(reentry);
        state.reentries.sort((a, b) => {
          return moment(a.timestamp) - moment(b.timestamp);
        });
      }
    },

    createConjunction: (state, action) => {
      let { conjunction } = action.payload;
      if (!doesIdExistInArray(state.conjunctions, conjunction.id)) {
        state.conjunctions.push(conjunction);
        state.conjunctions.sort((a, b) => {
          return moment(a.timestamp) - moment(b.timestamp);
        });
      }
    },

    pushSystemUpdate: (state, action) => {
      let { system } = action.payload;
      state.systems = state.systems.map((item) => {
        if (item.id === system.id) item.status = system.status;
        return item;
      });
    },

    pushLaunchUpdate: (state, action) => {
      let { launch } = action.payload;
      state.launches = state.launches.map((item) => {
        // if old id === new id, return new data instead
        return item.id === launch.id ? launch : item;
      });
    },

    pushConjunctionUpdate: (state, action) => {
      let { conjunction } = action.payload;
      state.conjunctions = state.conjunctions.map((item) => {
        // if old id === new id, return new data instead
        return item.id === conjunction.id ? conjunction : item;
      });
    },

    pushReentryUpdate: (state, action) => {
      let { reentry } = action.payload;
      state.reentries = state.reentries.map((item) => {
        // if old id === new id, return new data instead
        return item.id === reentry.id ? reentry : item;
      });
    },
  },

  extraReducers: (builder) => {
    builder.addCase(putLaunchIntel.fulfilled, (state, action) => {
      let { id, intel } = action.payload;
      state.launches = state.launches.map((item) => {
        // if old id === new id, return new data instead
        if (item.id === id) item.intel = intel;
        return item;
      });
    });

    builder.addCase(putReentryIntel.fulfilled, (state, action) => {
      let { id, intel } = action.payload;
      state.reentries = state.reentries.map((item) => {
        // if old id === new id, return new data instead
        if (item.id === id) item.intel = intel;
        return item;
      });
    });

    builder.addCase(putConjunctionIntel.fulfilled, (state, action) => {
      let { id, intel } = action.payload;
      state.conjunctions = state.conjunctions.map((item) => {
        // if old id === new id, return new data instead
        if (item.id === id) item.intel = intel;
        return item;
      });
    });
  },
});

export const {
  setData,
  createConjunction,
  createLaunch,
  createReentry,
  pushSystemUpdate,
  pushLaunchUpdate,
  pushConjunctionUpdate,
  pushReentryUpdate,
} = dataSlice.actions;

export default dataSlice.reducer;
