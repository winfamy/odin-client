import { configureStore } from "@reduxjs/toolkit";

import countrySlice from "./country-slice";
import dataSlice from "./data-slice";
import modalSlice from "./modal-slice";
import showIntelSlice from "./show-intel-slice";

export default configureStore({
  reducer: {
    country: countrySlice,
    data: dataSlice,
    modal: modalSlice,
    showIntel: showIntelSlice,
  },
});
