import Countdown, { zeroPad } from "react-countdown";
import PropTypes from "prop-types";
import "./launch-item.css";
import moment from "moment";
import { handleLaunchItemClick } from "../../../store/modal-slice";
import { useDispatch, useSelector } from "react-redux";

export const LaunchItem = (props) => {
  const showIntel = useSelector((state) => state.showIntel);
  const dispatch = useDispatch();

  const classes = "static text-sm font-normal text-left p-1 px-2 text-nowrap";
  const countdownClasses =
    "text-md font-semibold text-center p-1 rounded w-9 inline-block mr-1";

  const intelSplit = props.item.intel?.split("\n");
  let truncated = intelSplit.length > 3;
  const intelToShow = intelSplit.slice(0, 3).join("\n");

  const handleTrClick = (event) => {
    dispatch(handleLaunchItemClick({ launch_id: props.item.id }));
  };

  return (
    <>
      <tr
        className="relative hover:bg-slate-700 hover:cursor-pointer"
        onClick={handleTrClick}
      >
        <td className={classes}>
          <div className="flex flex-col p-1 shrink">
            <span className="text-xs text-slate-500 text-nowrap">
              {moment(props.item.timestamp).format("ddd, MMM Do YYYY")}
            </span>
            <span className="text-sm text-slate-300 text-nowrap">
              {moment(props.item.timestamp).format("hh:mm:ssZ")}
            </span>
          </div>
        </td>
        <td className={`${classes}`}>
          <Countdown
            date={moment(props.item.timestamp).toDate()}
            precision={2}
            overtime={true}
            renderer={({ days, hours, minutes, seconds }) => {
              let timebasedClasses = `${countdownClasses} ${
                moment(props.item.timestamp).isBefore()
                  ? "bg-red-500"
                  : "bg-green-500"
              }`;
              return (
                <>
                  <span className={timebasedClasses}>{zeroPad(days)}d</span>
                  <span className={timebasedClasses}>{zeroPad(hours)}h</span>
                  <span className={timebasedClasses}>{zeroPad(minutes)}m</span>
                  <span className={timebasedClasses}>{zeroPad(seconds)}s</span>
                </>
              );
            }}
          />
        </td>
        <td className={classes}>{props.item.description}</td>
        <td className={classes}>{props.item.site}</td>
        <td className={classes}>{props.item.type}</td>
        <td className={`${classes} text-center`}>
          <span className={`fi fi-${props.item.country}`}></span>
          <span className="uppercase"> {props.item.country}</span>
        </td>
        <td className={`${classes} text-center`}>
          <span className={`fi fi-${props.item.source}`}></span>
          <span className="uppercase"> {props.item.source}</span>
        </td>
      </tr>

      {showIntel && props.item.intel !== "" ? (
        <>
          <div className="mr-[-100vw]">
            <pre className="flex text-xs p-2 ml-4 w-1/2">
              {intelToShow}
              {truncated ? (
                <>
                  <br />
                  ...
                </>
              ) : null}
            </pre>
          </div>
        </>
      ) : (
        <></>
      )}
    </>
  );
};

LaunchItem.propTypes = {
  item: PropTypes.object,
};
