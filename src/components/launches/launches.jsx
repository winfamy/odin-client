import PropTypes from "prop-types";
import "./launches.css";
import { LaunchItem } from "./launch-item";
import { useSelector } from "react-redux";

export const Launches = (props) => {
  return (
    <div className="mt-4">
      <div className="not-prose relative rounded-xl overflow-hidden">
        <div className="table-bg absolute inset-0 bg-gradient-to-r from-cyan-500 to-blue-500 [mask-image:linear-gradient(0deg,rgba(255,255,255,0.1),rgba(255,255,255,0.5))]"></div>
        <div className="relative rounded-xl overflow-hidden">
          <div className="px-4 pt-0 pb-4">
            <div className="p-2 font-black uppercase font-sans text-2xl drop-shadow-sm">
              <span>LAUNCHES</span>
            </div>
            <table className="table-auto border-collapse w-full border border-slate-500 bg-slate-800 text-sm shadow-sm">
              <thead className="bg-slate-700">
                <tr>
                  <th className="w-px border px-2 py-2 font-semibold border-slate-500 text-left">
                    Time
                  </th>
                  <th className="w-px border px-2 py-2 font-semibold border-slate-500 text-left">
                    Countdown
                  </th>
                  <th className="border px-2 py-2 font-semibold border-slate-500 text-left">
                    Description
                  </th>
                  <th className="border px-2 py-2 font-semibold border-slate-500 text-left">
                    Site
                  </th>
                  <th className="border px-2 py-2 font-semibold border-slate-500 text-left w-px">
                    Type
                  </th>
                  <th className="border px-2 py-2 font-semibold border-slate-500 text-left w-px">
                    Country
                  </th>
                  <th className="border px-2 py-2 font-semibold border-slate-500 text-left w-px text-nowrap">
                    Source
                  </th>
                </tr>
              </thead>
              <tbody>
                {props.launch_data.map((item) => (
                  <LaunchItem key={item.id} item={item}></LaunchItem>
                ))}
              </tbody>
            </table>
          </div>
        </div>
        <div className="absolute inset-0 pointer-events-none border border-black/5 rounded-xl dark:border-white/5"></div>
      </div>
    </div>
  );
};

Launches.propTypes = {
  launch_data: PropTypes.array,
};
