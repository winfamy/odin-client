import PropTypes from "prop-types";
import "./conjunctions.css";
import { ConjunctionItem } from "./conjunction-item/conjunction-item";
import { ConjunctionChip } from "./conjunction-chip/conjunction-chip";

export const Conjunctions = (props) => {
  return (
    <div className="mt-4">
      <div className="not-prose relative rounded-xl overflow-hidden">
        <div className="table-bg absolute inset-0 bg-gradient-to-r from-cyan-500 to-blue-500 [mask-image:linear-gradient(0deg,rgba(255,255,255,0.1),rgba(255,255,255,0.5))]"></div>
        <div className="relative rounded-xl overflow-auto">
          <div className="px-4 pt-0 pb-4">
            <div className="p-2 font-black uppercase font-sans text-2xl drop-shadow-sm">
              <span>CONJUNCTIONS</span>
            </div>
            <table className="table-auto border-collapse w-full border border-slate-500 bg-slate-800 text-sm shadow-sm">
              <thead className="bg-slate-700">
                <tr>
                  <th className="w-px border px-2 py-2 font-semibold border-slate-500 text-left">
                    Time
                  </th>
                  <th className="w-px border px-2 py-2 font-semibold border-slate-500 text-left">
                    Countdown
                  </th>
                  <th className="w-px border px-2 py-2 font-semibold border-slate-500 text-left">
                    Concern
                  </th>
                  <th className="border px-2 py-2 font-semibold border-slate-500 text-left">
                    Primary
                  </th>
                  <th className="border px-2 py-2 font-semibold border-slate-500 text-left">
                    Secondary
                  </th>
                  <th className="border px-2 py-2 font-semibold border-slate-500 text-left">
                    Source
                  </th>
                </tr>
              </thead>
              <tbody>
                {props.conjunction_data.map((item) => (
                  <ConjunctionItem key={item.id} item={item}></ConjunctionItem>
                ))}
              </tbody>
            </table>
          </div>
        </div>
        <div className="absolute inset-0 pointer-events-none border border-black/5 rounded-xl dark:border-white/5"></div>
      </div>
    </div>
  );
};

Conjunctions.propTypes = {
  conjunction_data: PropTypes.array,
};
