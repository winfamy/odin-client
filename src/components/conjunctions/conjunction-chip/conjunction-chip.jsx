import PropTypes from "prop-types";
import "./conjunction-chip.css";

export const ConjunctionChip = (props) => {
  let color = {
    low: "yellow",
    medium: "orange",
    high: "red",
    emergency: "purple",
  }[props.level];

  return (
    <div
      className={`flex shrink select-none items-center whitespace-nowrap rounded-lg bg-${color}-500 py-1.5 px-3 font-sans text-xs font-semibold justify-center uppercase text-white`}
    >
      <span className="text-center">{props.level}</span>
    </div>
  );
};

ConjunctionChip.propTypes = { level: PropTypes.string };
