import { useState, Fragment, useEffect } from "react";
import { Dialog, Transition } from "@headlessui/react";
import "./reentry-modal.css";
import { useSelector, useDispatch } from "react-redux";
import {
  addData,
  putLaunchIntel,
  putReentryIntel,
} from "../../../store/data-slice";
import useKeybind from "../../../hooks/useKeybind";
import { modalClose, modalOpen } from "../../../store/modal-slice";

export function ReentryModal() {
  const dispatch = useDispatch();
  const reentry_id = useSelector((state) => state.modal.reentry_id);
  const reentry = useSelector((state) =>
    state.data.reentries.find((reentry) => reentry.id === reentry_id)
  );
  const isOpen = useSelector((state) => state.modal.modalOpen === "reentry");
  const [isEditing, setEditing] = useState(false);
  const [message, setMessage] = useState("");

  const handleMessageChange = (event) => {
    event.preventDefault();
    event.stopPropagation();
    setMessage(event.target.value);
  };

  function closeModal() {
    setEditing(false);
    dispatch(modalClose());
  }

  function openModal() {
    setIsOpen(true);
  }

  function applyIntel() {
    dispatch(putReentryIntel({ id: reentry.id, intel: message }));
    setEditing(false);
  }

  useEffect(() => {
    setMessage(reentry?.intel);
  }, [reentry]);

  return (
    <>
      <Dialog
        as="div"
        className="dialog w-1/3"
        open={isOpen}
        onClose={() => {
          dispatch(modalClose());
        }}
      >
        <Transition appear show={isOpen} as={Fragment}>
          <Dialog as="div" className="relative z-10" onClose={closeModal}>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <div className="fixed inset-0 bg-black/25" />
            </Transition.Child>

            <div className="fixed inset-0 overflow-y-auto items-center text-center">
              <div className="flex min-h-full w-1/2 items-center justify-center p-4 text-center m-auto">
                <Transition.Child
                  as={Fragment}
                  enter="ease-out duration-300"
                  enterFrom="opacity-0 scale-95"
                  enterTo="opacity-100 scale-100"
                  leave="ease-in duration-200"
                  leaveFrom="opacity-100 scale-100"
                  leaveTo="opacity-0 scale-95"
                >
                  <Dialog.Panel className="w-full transform overflow-hidden rounded-2xl bg-slate-700 p-3 text-left align-middle shadow-xl transition-all">
                    <div className="text-lg font-medium text-slate-100">
                      {reentry?.description}
                      <div className="text-slate-400 text-xs">
                        REENTRY INTEL{isEditing ? <> - EDITING</> : null}
                      </div>
                    </div>

                    <div>
                      <div className="p-4">
                        {isEditing ? (
                          <textarea
                            className="w-full text-xs min-h-20 flex mt-2 rounded text-black bg-white focus-visible:ring-1 p-1 mb-2"
                            value={message}
                            onChange={handleMessageChange}
                          ></textarea>
                        ) : (
                          <pre className="min-h-20 leading-8">
                            {reentry?.intel}
                          </pre>
                        )}
                      </div>

                      {!isEditing ? (
                        <button
                          type="button"
                          className="mr-2 bg-black/20 px-4 py-2 text-sm font-medium text-white hover:bg-black/30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white/75 rounded"
                          onClick={() => {
                            setEditing(true);
                          }}
                        >
                          Edit
                        </button>
                      ) : (
                        <></>
                      )}

                      {isEditing ? (
                        <button
                          type="button"
                          className="mr-2 bg-black/20 px-4 py-2 text-sm font-medium text-white hover:bg-black/30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white/75 rounded"
                          onClick={applyIntel}
                        >
                          Apply
                        </button>
                      ) : (
                        <></>
                      )}

                      {isEditing ? (
                        <button
                          type="button"
                          className="mr-2 bg-black/20 px-4 py-2 text-sm font-medium text-white hover:bg-black/30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white/75 rounded"
                          onClick={closeModal}
                        >
                          Save & Close
                        </button>
                      ) : (
                        <></>
                      )}
                    </div>
                  </Dialog.Panel>
                </Transition.Child>
              </div>
            </div>
          </Dialog>
        </Transition>
      </Dialog>
    </>
  );
}
