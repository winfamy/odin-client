import PropTypes from "prop-types";
import Countdown, { zeroPad } from "react-countdown";
import "./reentry-item.css";
import moment from "moment";
import { handleReentryClick } from "../../../store/modal-slice";
import { useDispatch, useSelector } from "react-redux";

const convertType = (input) => {
  if (input === "rocket-body") return "Rocket Body";
  return input;
};

export const ReentryItem = (props) => {
  const dispatch = useDispatch();
  const classes = "text-sm font-normal text-left p-1 px-2 text-nowrap";
  const countdownClasses =
    "text-md font-semibold text-center p-1 rounded w-9 inline-block mr-1";

  const handleTrClick = (event) => {
    dispatch(handleReentryClick({ reentry_id: props.item.id }));
  };

  const showIntel = useSelector((state) => state.showIntel);
  const intelSplit = props.item.intel?.split("\n") || [];
  let truncated = intelSplit.length > 3;
  const intelToShow = intelSplit.slice(0, 3).join("\n");

  return (
    <>
      <tr className="hover:bg-slate-700 cursor-pointer" onClick={handleTrClick}>
        <td className={classes}>
          <div className="flex flex-col p-1 shrink">
            <span className="text-xs text-slate-500 text-nowrap">
              {moment(props.item.timestamp).format("ddd, MMM Do YYYY")}
            </span>
            <span className="text-sm text-slate-300 text-nowrap">
              {moment(props.item.timestamp).format("hh:mm:ssZ")}
            </span>
          </div>
        </td>
        <td className={`${classes} w-px`}>
          <Countdown
            date={moment(props.item.timestamp).toDate()}
            precision={2}
            overtime={true}
            renderer={({ days, hours, minutes, seconds }) => {
              let timebasedClasses = `${countdownClasses} ${
                moment(props.item.timestamp).isBefore()
                  ? "bg-red-500"
                  : "bg-green-500"
              }`;
              return (
                <>
                  <span className={timebasedClasses}>{zeroPad(days)}d</span>
                  <span className={timebasedClasses}>{zeroPad(hours)}h</span>
                  <span className={timebasedClasses}>{zeroPad(minutes)}m</span>
                  <span className={timebasedClasses}>{zeroPad(seconds)}s</span>
                </>
              );
            }}
          />
        </td>
        <td className={classes}>
          <div className="flex flex-col">
            <span className="text-slate-500 text-xs">
              {props.item.object_id}
            </span>
            <span className="text-slate-300 text-sm">
              {props.item.object_name}
            </span>
          </div>
        </td>
        <td className={`capitalize ${classes}`}>
          {convertType(props.item.type)}
        </td>
        <td className={`${classes} text-center w-px`}>
          <span className={`fi fi-${props.item.source}`}></span>
          <span className="uppercase"> {props.item.source}</span>
        </td>
      </tr>
      {showIntel && props.item.intel ? (
        <>
          <pre className="flex text-xs p-2 ml-4 w-1/2 mr-[-100vw]">
            {intelToShow}
            {truncated ? (
              <>
                <br />
                ...
              </>
            ) : null}
          </pre>
        </>
      ) : (
        <></>
      )}
    </>
  );
};

ReentryItem.propTypes = {
  item: PropTypes.object,
};
