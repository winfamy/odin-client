import PropTypes from "prop-types";
import "./reentries.css";
import { ReentryItem } from "./reentry-item/reentry-item";

export const Reentries = (props) => {
  return (
    <div className="mt-4">
      <div className="not-prose relative rounded-xl overflow-hidden">
        <div className="table-bg absolute inset-0 bg-gradient-to-r from-cyan-500 to-blue-500 [mask-image:linear-gradient(0deg,rgba(255,255,255,0.1),rgba(255,255,255,0.5))]"></div>
        <div className="relative rounded-xl overflow-auto ">
          <div className="px-4 pt-0 pb-4">
            <div className="p-2 font-black uppercase font-sans text-2xl drop-shadow-sm">
              <span className="drop-shadow-md">RE-ENTRIES</span>
            </div>
            <table className="table-auto border-collapse w-full border border-slate-500 bg-slate-800 text-sm shadow-sm">
              <thead className="bg-slate-700">
                <tr>
                  <th className="w-px border px-2 py-2 font-semibold border-slate-500 text-left">
                    Time
                  </th>
                  <th className="w-px border px-2 py-2 font-semibold border-slate-500 text-left">
                    Countdown
                  </th>
                  <th className="border px-2 py-2 font-semibold border-slate-500 text-left">
                    Object
                  </th>
                  <th className="border px-2 py-2 font-semibold border-slate-500 text-left">
                    Type
                  </th>
                  <th className="w-px border px-2 py-2 font-semibold border-slate-500 text-left text-nowrap">
                    Source
                  </th>
                </tr>
              </thead>
              <tbody>
                {props.reentry_data.map((item) => (
                  <ReentryItem key={item.id} item={item}></ReentryItem>
                ))}
              </tbody>
            </table>
          </div>
        </div>
        <div className="absolute inset-0 pointer-events-none border border-black/5 rounded-xl dark:border-white/5"></div>
      </div>
    </div>
  );
};

Reentries.propTypes = {
  reentry_data: PropTypes.array,
};
