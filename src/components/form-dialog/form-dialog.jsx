import { useState, Fragment } from "react";
import { Dialog, Transition } from "@headlessui/react";
import "./form-dialog.css";
import { useSelector, useDispatch } from "react-redux";
import { addData } from "../../store/data-slice";
import useKeybind from "../../hooks/useKeybind";
import { modalClose, modalOpen } from "../../store/modal-slice";

export function FormDialog() {
  const dispatch = useDispatch();
  const country = useSelector((state) => state.country);
  const currentModal = useSelector((state) => state.modal.modalOpen);
  const isOpen = useSelector((state) => state.modal.modalOpen === "add-data");

  const [message, setMessage] = useState("");

  const handleMessageChange = (event) => {
    setMessage(event.target.value);
  };

  function closeModal() {
    dispatch(addData({ data: message, country: country.value }));
    setMessage("");
    dispatch(modalClose());
  }

  function openModal() {
    dispatch(modalOpen({ modalOpen: "add-data" }));
  }

  useKeybind(["a"], (event) => {
    if (!isOpen && currentModal === "") {
      event.preventDefault();
      setMessage("");
      dispatch(modalOpen({ modalOpen: "add-data" }));
    }
  });

  return (
    <>
      <button
        type="button"
        onClick={openModal}
        className="rounded-md bg-black/20 px-4 py-2 text-xs font-medium text-white hover:bg-black/30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white/75"
      >
        <span className="underline">A</span>dd Data
      </button>

      <Dialog
        as="div"
        className="dialog w-[30%]"
        open={isOpen}
        onClose={() => setIsOpen(false)}
      >
        <Transition appear show={isOpen} as={Fragment}>
          <Dialog as="div" className="relative z-10" onClose={closeModal}>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <div className="fixed inset-0 bg-black/25" />
            </Transition.Child>

            <div className="fixed inset-0 overflow-y-auto">
              <div className="flex min-h-full items-center justify-center p-4 text-center">
                <Transition.Child
                  as={Fragment}
                  enter="ease-out duration-300"
                  enterFrom="opacity-0 scale-95"
                  enterTo="opacity-100 scale-100"
                  leave="ease-in duration-200"
                  leaveFrom="opacity-100 scale-100"
                  leaveTo="opacity-0 scale-95"
                >
                  <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-slate-600 p-6 text-left align-middle shadow-xl transition-all">
                    <Dialog.Title
                      as="h3"
                      className="text-lg font-medium leading-6 text-slate-100"
                    >
                      Add Data
                    </Dialog.Title>
                    <div className="mt-2">
                      <p className="text-sm text-slate-100">
                        Copy and paste data to add here.
                      </p>
                    </div>

                    <textarea
                      className="flex w-full h-[120px] bg-slate-700 focus-visible:ring-slate-400 focus-visible:ring-1 focus:outline-none rounded outline-slate-800 outline-1 outline text-xs mt-2 p-1"
                      value={message}
                      onChange={handleMessageChange}
                    ></textarea>

                    <div className="mt-4">
                      <button
                        type="button"
                        className="bg-black/20 px-4 py-2 text-sm font-medium text-white hover:bg-black/30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white/75"
                        onClick={closeModal}
                      >
                        Submit
                      </button>
                    </div>
                  </Dialog.Panel>
                </Transition.Child>
              </div>
            </div>
          </Dialog>
        </Transition>
      </Dialog>
    </>
  );
}
