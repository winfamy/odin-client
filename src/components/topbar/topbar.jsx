import { FormDialog } from "../form-dialog/form-dialog";
import { Dropdown } from "./dropdown/dropdown.jsx";
import { CreateLaunchModal } from "../create-modals/create-launch-modal/create-launch-modal.jsx";
import { CreateConjunctionModal } from "../create-modals/create-conjunction-modal/create-conjunction-modal";
import { CreateReentryModal } from "../create-modals/create-reentry-modal/create-reentry-modal";
import { ToggleIntel } from "./toggle-intel/toggle-intel";

import "./topbar.css";

function Topbar() {
  const classification = import.meta.env.VITE_CLASSIFICATION;

  return (
    <>
      {classification === "unclassified" ? (
        <div className="flex font-semibold bg-green-600 text-white justify-center w-auto">
          <span>UNCLASSIFIED</span>
        </div>
      ) : (
        <div className="flex font-semibold bg-red-600 text-white justify-center w-auto">
          <span>SECRET // REL FVEY</span>
        </div>
      )}
      <nav className="flex items-center justify-between flex-wrap bg-slate-800 p-6 border-b border-slate-50/[0.06]">
        <div className="flex items-center flex-shrink-0 text-white mr-6">
          <svg
            className="fill-current h-8 w-8 mr-2"
            width="54"
            height="54"
            viewBox="0 0 54 54"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M13.5 22.1c1.8-7.2 6.3-10.8 13.5-10.8 10.8 0 12.15 8.1 17.55 9.45 3.6.9 6.75-.45 9.45-4.05-1.8 7.2-6.3 10.8-13.5 10.8-10.8 0-12.15-8.1-17.55-9.45-3.6-.9-6.75.45-9.45 4.05zM0 38.3c1.8-7.2 6.3-10.8 13.5-10.8 10.8 0 12.15 8.1 17.55 9.45 3.6.9 6.75-.45 9.45-4.05-1.8 7.2-6.3 10.8-13.5 10.8-10.8 0-12.15-8.1-17.55-9.45-3.6-.9-6.75.45-9.45 4.05z" />
          </svg>
          <span className="font-semibold text-xl tracking-tight">ODIN</span>
        </div>
        <div className="flex-grow flex items-center w-auto">
          <div className="text-sm lg:flex-grow">
            <a
              href="#responsive-header"
              className="block mt-4 lg:inline-block lg:mt-0 text-white hover:text-sky-500 mr-4"
            >
              Nav 1
            </a>
            <a
              href="#responsive-header"
              className="block mt-4 lg:inline-block lg:mt-0 text-white hover:text-sky-500 mr-4"
            >
              Nav 2
            </a>
            <a
              href="#responsive-header"
              className="block mt-4 lg:inline-block lg:mt-0 text-white hover:text-sky-500"
            >
              Nav 3
            </a>
          </div>
          <div>
            <a
              href="#"
              className="inline-block text-sm text-white mr-4 hover:text-sky-500"
            >
              <Dropdown></Dropdown>
            </a>

            <ToggleIntel></ToggleIntel>
            <CreateReentryModal></CreateReentryModal>
            <CreateLaunchModal></CreateLaunchModal>
            <CreateConjunctionModal></CreateConjunctionModal>
            <FormDialog></FormDialog>
          </div>
        </div>
      </nav>
    </>
  );
}

export default Topbar;
