import PropTypes from "prop-types";
import "./toggle-intel.css";
import useKeybind from "../../../hooks/useKeybind";
import { useDispatch, useSelector } from "react-redux";
import { toggleShowIntel } from "../../../store/show-intel-slice";

export const ToggleIntel = (props) => {
  const hotkey = "i";
  const dispatch = useDispatch();

  const isModalOpen = useSelector((state) => state.modal.modalOpen !== "");

  function toggleIntel() {
    if (!isModalOpen) {
      dispatch(toggleShowIntel());
    }
  }

  useKeybind([hotkey], toggleIntel);

  return (
    <>
      <button
        type="button"
        className="mr-2 rounded-md bg-black/20 px-4 py-2 text-xs font-medium text-white hover:bg-black/30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white/75"
        onClick={toggleIntel}
      >
        Toggle <span className="underline">I</span>ntel
      </button>
    </>
  );
};

ToggleIntel.propTypes = {};
