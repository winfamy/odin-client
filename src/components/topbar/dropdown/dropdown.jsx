import { Menu, Transition } from "@headlessui/react";
import { Fragment, useEffect, useRef, useState } from "react";
import "./dropdown.css";

import { setCountry } from "../../../store/country-slice";
import { useSelector, useDispatch } from "react-redux";

export const Dropdown = (props) => {
  const country = useSelector((state) => state.country);
  const dispatch = useDispatch();

  return (
    <div className="relative text-right z-50">
      <Menu as="div" className="text-left">
        <div>
          <Menu.Button className="flex justify-center rounded-md bg-black/20 text-sm font-medium text-white hover:bg-black/30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white/75 h-full">
            <span className={`fi fi-${country.value} mr-1`}></span>
          </Menu.Button>
        </div>
        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Menu.Items className="absolute right-0 mt-2 w-56 origin-top-right divide-y divide-gray-100 rounded-md bg-slate-900 shadow-lg ring-1 ring-black/5 focus:outline-none">
            <div className="px-1 py-1">
              <Menu.Item>
                {({ active }) => (
                  <button
                    className={`${
                      active ? "bg-sky-500 text-white" : "text-white"
                    } group flex w-full items-center rounded-md px-2 py-2 text-sm`}
                    onClick={() => dispatch(setCountry("us"))}
                  >
                    <span className="fi fi-us mr-1"></span> United States
                  </button>
                )}
              </Menu.Item>
              <Menu.Item onClick={(event) => event.stopPropagation()}>
                {({ active }) => (
                  <button
                    className={`${
                      active ? "bg-sky-500 text-white" : "text-white"
                    } group flex w-full items-center rounded-md px-2 py-2 text-sm`}
                    onClick={() => dispatch(setCountry("gb"))}
                  >
                    <span className="fi fi-gb mr-1"></span> United Kingdom
                  </button>
                )}
              </Menu.Item>
              <Menu.Item>
                {({ active }) => (
                  <button
                    className={`${
                      active ? "bg-sky-500 text-white" : "text-white"
                    } group flex w-full items-center rounded-md px-2 py-2 text-sm`}
                    onClick={() => dispatch(setCountry("ca"))}
                  >
                    <span className="fi fi-ca mr-1"></span> Canada
                  </button>
                )}
              </Menu.Item>
              <Menu.Item>
                {({ active }) => (
                  <button
                    className={`${
                      active ? "bg-sky-500 text-white" : "text-white"
                    } group flex w-full items-center rounded-md px-2 py-2 text-sm`}
                    onClick={() => dispatch(setCountry("jp"))}
                  >
                    <span className="fi fi-jp mr-1"></span> Japan
                  </button>
                )}
              </Menu.Item>
              <Menu.Item>
                {({ active }) => (
                  <button
                    className={`${
                      active ? "bg-sky-500 text-white" : "text-white"
                    } group flex w-full items-center rounded-md px-2 py-2 text-sm`}
                    onClick={() => dispatch(setCountry("de"))}
                  >
                    <span className="fi fi-de mr-1"></span> Germany
                  </button>
                )}
              </Menu.Item>
            </div>
          </Menu.Items>
        </Transition>
      </Menu>
    </div>
  );
};

Dropdown.propTypes = {};
