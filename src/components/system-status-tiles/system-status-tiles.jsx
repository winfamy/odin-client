import PropTypes from "prop-types";
import "./system-status-tiles.css";
import { useSelector, useDispatch } from "react-redux";
import { updateSystem } from "../../store/data-slice";

export const SystemStatusTiles = (props) => {
  const currentCountry = useSelector((state) => state.country);
  const dispatch = useDispatch();

  const byCountry = Object.groupBy(props.system_data, ({ country }) => country);
  let system_data = {};
  Object.keys(byCountry).map((country) => {
    let countryData = byCountry[country];
    system_data[country] = Object.groupBy(
      countryData,
      ({ acronym_group }) => acronym_group
    );
  });

  const colors = {
    red: "bg-red-500",
    green: "bg-green-500",
    yellow: "bg-yellow-500",
  };

  return (
    <>
      {Object.keys(system_data).map((country, idx) => {
        let pointer = currentCountry.value === country ? "cursor-pointer" : "";

        return (
          <div key={idx} className="flex flex-row flex-grow justify-left mr-12">
            <div className="flex flex-col flex-shrink flex-nowrap w-10 justify-center items-left mr-4">
              <div className="flex flex-row">
                <div
                  className={`flex fib fi-${country} w-[60px] h-[60px]`}
                ></div>
              </div>
            </div>

            <div className="flex-col">
              {Object.keys(system_data[country]).map((header) => {
                if (header !== "") {
                  return (
                    <div
                      key={header}
                      className="container flex items-center my-1"
                    >
                      <span className="text-slate-400 text-xs status-tile-header text-right mr-1">
                        {header}
                      </span>

                      {system_data[country][header].map((system, index) => (
                        <div
                          key={"system" + index}
                          className={`status-tile ${
                            colors[system.status]
                          } ${pointer} mx-[2px] rounded-[2px]`}
                          onClick={() => {
                            if (country === currentCountry.value) {
                              dispatch(updateSystem(system.id));
                            }
                          }}
                        ></div>
                      ))}
                    </div>
                  );
                }
              })}

              <div className="grid grid-cols-6 gap-0">
                {system_data[country][""]?.map((system, idx) => (
                  <div
                    key={idx}
                    className="container flex flex-col items-center justify-center my-[2px]"
                  >
                    <div
                      className={`status-tile ${
                        colors[system.status]
                      } ${pointer} mx-[2px] rounded-[2px]`}
                      onClick={() => {
                        if (country === currentCountry.value) {
                          dispatch(updateSystem(system.id));
                        }
                      }}
                    ></div>

                    <span className="text-slate-400 text-xs status-tile-header text-center">
                      {system.acronym}
                    </span>
                  </div>
                ))}
              </div>
            </div>
          </div>
        );
      })}
    </>
  );
};

SystemStatusTiles.propTypes = {
  system_data: PropTypes.array,
};
