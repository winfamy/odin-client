import PropTypes from "prop-types";
import { useState, Fragment, useEffect } from "react";
import { Dialog, Transition } from "@headlessui/react";
import "./create-reentry-modal.css";
import { useSelector, useDispatch } from "react-redux";
import { addData, createReentryFromModal } from "../../../store/data-slice";
import useKeybind from "../../../hooks/useKeybind";
import { modalClose, modalOpen } from "../../../store/modal-slice";
import moment from "moment";

export const CreateReentryModal = (props) => {
  const CREATE_REENTRY_MODAL = "create-reentry-modal";
  const hotkey = "r";

  const dispatch = useDispatch();
  const country = useSelector((state) => state.country.value);
  const currentModal = useSelector((state) => state.modal.modalOpen);
  const isOpen = useSelector(
    (state) => state.modal.modalOpen === CREATE_REENTRY_MODAL
  );

  const initFormData = {
    timestamp: moment().format("YYYY-MM-DDTHH:mm:ss"),
    object_id: "",
    object_name: "",
    type: "unknown",
    intel: "",
  };

  const [formData, setFormData] = useState(initFormData);

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevFormData) => {
      console.log(prevFormData);
      return { ...prevFormData, [name]: value };
    });
  };

  function closeModal() {
    setFormData(initFormData);
    dispatch(modalClose());
  }

  function openModal() {
    dispatch(modalOpen({ modalOpen: CREATE_REENTRY_MODAL }));
  }

  function submitModal() {
    dispatch(createReentryFromModal({ reentry: formData, country }));
    closeModal();
  }

  useKeybind([hotkey], (event) => {
    if (!isOpen && currentModal === "") {
      event.preventDefault();
      openModal();
    }
  });

  return (
    <>
      <button
        type="button"
        onClick={openModal}
        className="mr-2 rounded-md bg-black/20 px-4 py-2 text-xs font-medium text-white hover:bg-black/30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white/75"
      >
        Create <span className="underline">R</span>eentry
      </button>

      <Dialog
        as="div"
        className="dialog w-[30%]"
        open={isOpen}
        onClose={closeModal}
      >
        <Transition appear show={isOpen} as={Fragment}>
          <Dialog as="div" className="relative z-10" onClose={closeModal}>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <div className="fixed inset-0 bg-black/25" />
            </Transition.Child>

            <div className="fixed inset-0 overflow-y-auto">
              <div className="flex min-h-full items-center justify-center p-4 text-center">
                <Transition.Child
                  as={Fragment}
                  enter="ease-out duration-300"
                  enterFrom="opacity-0 scale-95"
                  enterTo="opacity-100 scale-100"
                  leave="ease-in duration-200"
                  leaveFrom="opacity-100 scale-100"
                  leaveTo="opacity-0 scale-95"
                >
                  <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-slate-600 p-6 text-left align-middle shadow-xl transition-all">
                    <Dialog.Title
                      as="h3"
                      className="text-md font-medium leading-6 text-slate-100 mb-4"
                    >
                      CREATE RE-ENTRY
                    </Dialog.Title>
                    <div className="flex justify-between mb-3">
                      <label
                        htmlFor="timestamp"
                        className="text-xs flex items-center mr-2"
                      >
                        Date & Time
                      </label>
                      <input
                        type="datetime-local"
                        name="timestamp"
                        step="1"
                        value={formData.timestamp}
                        onChange={handleChange}
                        className="flex w-2/3 p-1 bg-slate-700 focus-visible:ring-slate-400 focus-visible:ring-1 focus:outline-none rounded outline-slate-800 outline-1 outline"
                      />
                    </div>

                    <div className="flex justify-between mb-3">
                      <label
                        htmlFor="object_id"
                        className="text-xs flex items-center mr-"
                      >
                        Object ID
                      </label>
                      <input
                        type="text"
                        name="object_id"
                        value={formData.object_id}
                        onChange={handleChange}
                        maxLength={6}
                        className="flex w-2/3 p-1 bg-slate-700 focus-visible:ring-slate-400 focus-visible:ring-1 focus:outline-none rounded outline-slate-800 outline-1 outline"
                      />
                    </div>

                    <div className="flex justify-between mb-3">
                      <label
                        htmlFor="object_name"
                        className="text-xs flex items-center mr-"
                      >
                        Object Name
                      </label>
                      <input
                        type="text"
                        name="object_name"
                        value={formData.object_name}
                        onChange={handleChange}
                        className="flex w-2/3 p-1 bg-slate-700 focus-visible:ring-slate-400 focus-visible:ring-1 focus:outline-none rounded outline-slate-800 outline-1 outline"
                      />
                    </div>

                    <div className="flex justify-between mb-3">
                      <label
                        htmlFor="type"
                        className="text-xs flex items-center mr-2"
                      >
                        Type
                      </label>
                      <select
                        name="type"
                        value={formData.type}
                        onChange={handleChange}
                        className="flex w-2/3 p-1 bg-slate-700 focus-visible:ring-slate-400 focus-visible:ring-1 focus:outline-none rounded outline-slate-800 outline-1 outline"
                      >
                        <option value="unknown">Unknown</option>
                        <option value="debris">Debris</option>
                        <option value="payload">Payload</option>
                        <option value="rocket-body">Rocket Body</option>
                      </select>
                    </div>

                    <div className="mt-4">
                      <label
                        htmlFor="type"
                        className="text-xs flex items-center mb-2"
                      >
                        Intel
                      </label>
                      <textarea
                        name="intel"
                        value={formData.intel}
                        onChange={handleChange}
                        className="flex w-full h-[120px] bg-slate-700 focus-visible:ring-slate-400 focus-visible:ring-1 focus:outline-none rounded outline-slate-800 outline-1 outline text-xs mt-2 p-1"
                      ></textarea>
                    </div>

                    <div className="mt-4">
                      <button
                        type="button"
                        className="bg-black/20 px-4 py-2 text-sm font-medium text-white hover:bg-black/30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white/75"
                        onClick={submitModal}
                      >
                        Submit
                      </button>
                    </div>
                  </Dialog.Panel>
                </Transition.Child>
              </div>
            </div>
          </Dialog>
        </Transition>
      </Dialog>
    </>
  );
};

CreateReentryModal.propTypes = {};
