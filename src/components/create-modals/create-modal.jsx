import PropTypes from "prop-types";
import "./create-modal.css";

export const CreateLaunchModal = (props) => {
  let { modalId, hotkey, render, submitCallback } = props;

  const dispatch = useDispatch();
  const country = useSelector((state) => state.country);
  const currentModal = useSelector((state) => state.modal.modalOpen);
  const isOpen = useSelector((state) => state.modal.modalOpen === modalId);

  const [formData, setFormData] = useState({
    name: "",
    email: "",
    message: "",
  });

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevFormData) => ({ ...prevFormData, [name]: value }));
  };

  function closeModal() {
    dispatch(addData({ data: message, country: country.value }));
    setMessage("");
    dispatch(modalClose());
  }

  function openModal() {
    setMessage("");
  }

  useKeybind([hotkey], (event) => {
    if (!isOpen && currentModal === "") {
      event.preventDefault();
      setMessage("");
      dispatch(modalOpen({ modalOpen: modalId }));
    }
  });

  return (
    <>
      <button
        type="button"
        onClick={openModal}
        className="rounded-md bg-black/20 px-4 py-2 text-sm font-medium text-white hover:bg-black/30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white/75"
      >
        <span className="underline">A</span>dd Data
      </button>

      <Dialog
        as="div"
        className="dialog w-[30%]"
        open={isOpen}
        onClose={() => setIsOpen(false)}
      >
        <Transition appear show={isOpen} as={Fragment}>
          <Dialog as="div" className="relative z-10" onClose={closeModal}>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <div className="fixed inset-0 bg-black/25" />
            </Transition.Child>

            <div className="fixed inset-0 overflow-y-auto">
              <div className="flex min-h-full items-center justify-center p-4 text-center">
                <Transition.Child
                  as={Fragment}
                  enter="ease-out duration-300"
                  enterFrom="opacity-0 scale-95"
                  enterTo="opacity-100 scale-100"
                  leave="ease-in duration-200"
                  leaveFrom="opacity-100 scale-100"
                  leaveTo="opacity-0 scale-95"
                >
                  <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-slate-600 p-6 text-left align-middle shadow-xl transition-all">
                    <Dialog.Title
                      as="h3"
                      className="text-lg font-medium leading-6 text-slate-100"
                    >
                      Add Data
                    </Dialog.Title>
                    <div className="mt-2">
                      <p className="text-sm text-slate-100">
                        Copy and paste data to add here.
                      </p>
                    </div>

                    <textarea
                      className="w-full text-xs h-[60px] flex mt-2 rounded text-black bg-white focus-visible:ring-1 p-1"
                      value={message}
                      onChange={handleMessageChange}
                    ></textarea>

                    <div className="mt-4">
                      <button
                        type="button"
                        className="bg-black/20 px-4 py-2 text-sm font-medium text-white hover:bg-black/30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white/75"
                        onClick={closeModal}
                      >
                        <span className="underline">S</span>ubmit
                      </button>
                    </div>
                  </Dialog.Panel>
                </Transition.Child>
              </div>
            </div>
          </Dialog>
        </Transition>
      </Dialog>
    </>
  );
};

CreateModal.propTypes = {
  hotkey: PropTypes.string,
  render: PropTypes.node,
  submitCallback: PropTypes.func,
};
