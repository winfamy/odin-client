import PropTypes from "prop-types";
import { useState, Fragment, useEffect } from "react";
import { Dialog, Transition } from "@headlessui/react";
import "./create-launch-modal.css";
import { useSelector, useDispatch } from "react-redux";
import {
  addData,
  createLaunchFromModal,
  putLaunchIntel,
} from "../../../store/data-slice";
import useKeybind from "../../../hooks/useKeybind";
import { modalClose, modalOpen } from "../../../store/modal-slice";
import moment from "moment";

export const CreateLaunchModal = (props) => {
  const CREATE_LAUNCH_MODAL = "create-launch-modal";
  const hotkey = "l";

  const dispatch = useDispatch();
  const country = useSelector((state) => state.country.value);
  const currentModal = useSelector((state) => state.modal.modalOpen);
  const isOpen = useSelector(
    (state) => state.modal.modalOpen === CREATE_LAUNCH_MODAL
  );

  const initFormData = {
    timestamp: moment().format("YYYY-MM-DDTHH:mm:ss"),
    description: "",
    site: "",
    type: "",
    country: "",
    intel: "",
  };

  const [formData, setFormData] = useState(initFormData);

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevFormData) => {
      console.log(prevFormData);
      return { ...prevFormData, [name]: value };
    });
  };

  function closeModal() {
    setFormData(initFormData);
    dispatch(modalClose());
  }

  function openModal() {
    dispatch(modalOpen({ modalOpen: CREATE_LAUNCH_MODAL }));
  }

  function submitModal() {
    dispatch(createLaunchFromModal({ launch: formData, country }));
    closeModal();
  }

  useKeybind([hotkey], (event) => {
    if (!isOpen && currentModal === "") {
      event.preventDefault();
      openModal();
    }
  });

  return (
    <>
      <button
        type="button"
        onClick={openModal}
        className="mr-2 rounded-md bg-black/20 px-4 py-2 text-xs font-medium text-white hover:bg-black/30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white/75"
      >
        Create <span className="underline">L</span>aunch
      </button>

      <Dialog
        as="div"
        className="dialog w-[30%]"
        open={isOpen}
        onClose={closeModal}
      >
        <Transition appear show={isOpen} as={Fragment}>
          <Dialog as="div" className="relative z-10" onClose={closeModal}>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <div className="fixed inset-0 bg-black/25" />
            </Transition.Child>

            <div className="fixed inset-0 overflow-y-auto">
              <div className="flex min-h-full items-center justify-center p-4 text-center">
                <Transition.Child
                  as={Fragment}
                  enter="ease-out duration-300"
                  enterFrom="opacity-0 scale-95"
                  enterTo="opacity-100 scale-100"
                  leave="ease-in duration-200"
                  leaveFrom="opacity-100 scale-100"
                  leaveTo="opacity-0 scale-95"
                >
                  <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-slate-600 p-6 text-left align-middle shadow-xl transition-all">
                    <Dialog.Title
                      as="h3"
                      className="text-md font-medium leading-6 text-slate-100 mb-4"
                    >
                      CREATE LAUNCH
                    </Dialog.Title>
                    <div className="flex justify-between mb-3">
                      <label
                        htmlFor="timestamp"
                        className="text-xs flex items-center mr-2"
                      >
                        Date & Time
                      </label>
                      <input
                        type="datetime-local"
                        name="timestamp"
                        step="1"
                        value={formData.timestamp}
                        onChange={handleChange}
                        className="flex w-2/3 p-1 bg-slate-700 focus-visible:ring-slate-400 focus-visible:ring-1 focus:outline-none rounded outline-slate-800 outline-1 outline"
                      />
                    </div>

                    <div className="flex justify-between mb-3">
                      <label
                        htmlFor="description"
                        className="text-xs flex items-center mr-"
                      >
                        Description
                      </label>
                      <input
                        type="text"
                        name="description"
                        value={formData.description}
                        onChange={handleChange}
                        className="flex w-2/3 p-1 bg-slate-700 focus-visible:ring-slate-400 focus-visible:ring-1 focus:outline-none rounded outline-slate-800 outline-1 outline"
                      />
                    </div>

                    <div className="flex justify-between mb-3">
                      <label
                        htmlFor="site"
                        className="text-xs flex items-center mr-2"
                      >
                        Site
                      </label>
                      <input
                        type="text"
                        name="site"
                        value={formData.site}
                        onChange={handleChange}
                        className="flex w-2/3 p-1 bg-slate-700 focus-visible:ring-slate-400 focus-visible:ring-1 focus:outline-none rounded outline-slate-800 outline-1 outline"
                      />
                    </div>

                    <div className="flex justify-between mb-3">
                      <label
                        htmlFor="type"
                        className="text-xs flex items-center mr-2"
                      >
                        Type
                      </label>
                      <input
                        type="text"
                        name="type"
                        value={formData.type}
                        onChange={handleChange}
                        className="flex w-2/3 p-1 bg-slate-700 focus-visible:ring-slate-400 focus-visible:ring-1 focus:outline-none rounded outline-slate-800 outline-1 outline"
                      />
                    </div>

                    <div className="flex justify-between mb-3">
                      <label
                        htmlFor="type"
                        className="text-xs flex items-center mr-2"
                      >
                        Country (ISO 3166-1)
                      </label>
                      <input
                        type="text"
                        name="country"
                        value={formData.country}
                        onChange={handleChange}
                        maxLength={2}
                        className="flex w-2/3 p-1 bg-slate-700 focus-visible:ring-slate-400 focus-visible:ring-1 focus:outline-none rounded outline-slate-800 outline-1 outline"
                      />
                    </div>

                    <div className="mt-4">
                      <label
                        htmlFor="type"
                        className="text-xs flex items-center mb-2"
                      >
                        Intel
                      </label>
                      <textarea
                        name="intel"
                        value={formData.intel}
                        onChange={handleChange}
                        className="flex w-full p-1 bg-slate-700 focus-visible:ring-slate-400 focus-visible:ring-1 focus:outline-none rounded outline-slate-800 outline-1 outline"
                      ></textarea>
                    </div>

                    <div className="mt-4">
                      <button
                        type="button"
                        className="bg-black/20 px-4 py-2 text-sm font-medium text-white hover:bg-black/30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white/75"
                        onClick={submitModal}
                      >
                        Submit
                      </button>
                    </div>
                  </Dialog.Panel>
                </Transition.Child>
              </div>
            </div>
          </Dialog>
        </Transition>
      </Dialog>
    </>
  );
};

CreateLaunchModal.propTypes = {};
