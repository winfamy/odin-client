import { useState, useEffect } from "react";
import "./App.css";
import "/node_modules/flag-icons/css/flag-icons.min.css";
import Topbar from "./components/topbar/topbar";
import { SystemStatusTiles } from "./components/system-status-tiles/system-status-tiles";
import { Launches } from "./components/launches";
import moment from "moment/moment";
import { Conjunctions } from "./components/conjunctions/conjunctions";
import { Reentries } from "./components/reentries/reentries";
import { socket } from "./socket";
import { LaunchModal } from "./components/launches/launch-modal/launch-modal";
import { ReentryModal } from "./components/reentries/reentry-modal/reentry-modal";
import { ConjunctionModal } from "./components/conjunctions/conjunction-modal/conjunction-modal";
import { CreateLaunchModal } from "./components/create-modals/create-launch-modal/create-launch-modal";

import store from "./store/store";
import { Provider } from "react-redux";
import { useSelector, useDispatch } from "react-redux";
import {
  createConjunction,
  createLaunch,
  createReentry,
  pushConjunctionUpdate,
  pushLaunchUpdate,
  pushReentryUpdate,
  pushSystemUpdate,
  setData,
} from "./store/data-slice";

function App() {
  const [isConnected, setIsConnected] = useState(socket.connected);
  const [fooEvents, setFooEvents] = useState([]);
  const dispatch = useDispatch();
  const launch_data = useSelector((state) => state.data.launches);
  const conjunction_data = useSelector((state) => state.data.conjunctions);
  const reentry_data = useSelector((state) => state.data.reentries);
  const system_data = useSelector((state) => state.data.systems);

  useEffect(() => {
    function onConnect() {
      setIsConnected(true);
    }

    function onDisconnect() {
      setIsConnected(false);
    }

    socket.on("connect", onConnect);
    socket.on("disconnect", onDisconnect);
    socket.on("data", (data) => {
      dispatch(
        setData({
          launches: data.launches,
          reentries: data.reentries,
          conjunctions: data.conjunctions,
          systems: data.systems,
        })
      );
    });

    return () => {
      socket.off("connect", onConnect);
      socket.off("disconnect", onDisconnect);
      socket.off("data");
    };
  });

  socket.on("new_data", (data) => {
    console.log(data);
    switch (data.object_type) {
      case "reentry":
        let { reentry } = data;
        dispatch(createReentry(data));
        break;
      case "launch":
        let { launch } = data;
        dispatch(createLaunch(data));
        break;
      case "conjunction":
        let { conjunction } = data;
        dispatch(createConjunction(data));
        break;
    }
  });

  // ============================================================================================
  // THIS BLOCK IS FOR WHEN THE SYSTEM RECEIVES DATA UPDATES VIA WEBSOCKET
  // ============================================================================================
  socket.on("updated_system", ({ system }) => {
    dispatch(pushSystemUpdate({ system }));
  });

  socket.on("updated_reentry", ({ reentry }) => {
    dispatch(pushReentryUpdate({ reentry }));
  });

  socket.on("updated_conjunction", ({ conjunction }) => {
    dispatch(pushConjunctionUpdate({ conjunction }));
  });

  socket.on("updated_launch", ({ launch }) => {
    dispatch(pushLaunchUpdate({ launch }));
  });

  return (
    <>
      <Topbar></Topbar>

      <div className="w-full px-2 py-2 flex flex-col">
        <div className="flex flex-col p-2">
          <h2 className="ml-1">System Status</h2>
          <div className="flex flex-row">
            <SystemStatusTiles system_data={system_data}></SystemStatusTiles>
          </div>
        </div>

        <div className="flex flex-row">
          <div className="flex flex-col p-2 grow">
            <Launches launch_data={launch_data}></Launches>
          </div>

          <div className="flex flex-col p-2 grow">
            <Conjunctions conjunction_data={conjunction_data}></Conjunctions>
            <Reentries reentry_data={reentry_data}></Reentries>
          </div>
        </div>
      </div>

      <div>
        <LaunchModal></LaunchModal>
        <ReentryModal></ReentryModal>
        <ConjunctionModal></ConjunctionModal>
      </div>
    </>
  );
}

export default App;
